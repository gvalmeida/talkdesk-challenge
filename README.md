# Talkdesk Challenge: Phone Numbers by Area Code
This exercise is part of Talkdesk's recruitment process.

## Requirements
- NodeJs

## Running
```
node challenge.js input_file.txt
```

## Some notes
- no extra libraries needed. Only Node. (logging and testing can be easily added with winston and mocha library).
- there are some debug code helpers that are deactivated by default. Activating config.REPORT_INVALID_NUMBERS will impact performance and memory with large input files (if many invalid_numbers).
- only valid phone numbers with an existent prefix in area_codes.txt will be displayed in output. 000 is a valid number, but it will not be accounted because there is not an area code 0.
- '00' doesn't count to the maximum number of digits, but it counts for other digits. A phone number like 00911 will not be valid because it has 5 digits. If the total number of digits should be subtracted, the number 000 should not be valid (it would have 1 digit length without the 00).
- duplicate phone numbers are not checked and will be accumulated in output.