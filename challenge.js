var fs = require('fs');
var readline = require('readline');
var stream = require('stream');

var challengeApp = challengeApp || {};

// config values
challengeApp.config = {
	AREA_CODES_FILENAME: './area_codes.txt',

	// enable only for trace (accumulates all invalid numbers in memory)
	REPORT_INVALID_NUMBERS: false
};

// get file name from input
challengeApp.inputFileName = process.argv[2];

// This module contains all area code related data
challengeApp.areaCodeModule = (function(areaCodeFileName) {
	var areaCodes = {},
		fileText = fs.readFileSync(areaCodeFileName, 'utf8');

	// loads all valid areaCodes from file
	fileText.split(/\r?\n/).forEach(function(line) {
		areaCodes[line.trim()] = true;
	});

	// returs a valid area code from a valid phone number
	var parsePhoneNumberAreaCode = function(phoneNumber) {
		var areaCode = null,
			maxAreaCodeSize = Math.max(phoneNumber.slice(0, 3).length, 3),
			possibleAreaCode = phoneNumber.slice(0, maxAreaCodeSize);

		for (var areaCodeSize = maxAreaCodeSize; areaCodeSize >= 1; areaCodeSize--) {
			// check area codes starting from 3 digits to 1
			if (areaCodes.hasOwnProperty(possibleAreaCode.slice(0, areaCodeSize))) {
				areaCode = possibleAreaCode.slice(0, areaCodeSize);
				break;
			}
		}

		return areaCode;
	};

	return {
		areaCodes: areaCodes,
		parsePhoneNumberAreaCode: parsePhoneNumberAreaCode
	};

})(challengeApp.config.AREA_CODES_FILENAME);

//This module contains all phone numbers related data
challengeApp.phoneNumberModule = (function(inputFileName) {
	var phoneNumbersByAreaCodeList = {},
		inphoneNumbersByAreaCodeList = {},
		unknownAreaCodePhoneNumberList = {};

	// returns a valid phone number wihtout a '00' or a '+'
	// if a number is not valid raises an error
	var parsePhoneNumber = function(phoneNumber) {

		if (typeof phoneNumber !== 'string') {
			throw 'phoneNumber not a string';
		}

		// remove whitespaces in the edges
		phoneNumber = phoneNumber.trim();

		if (phoneNumber.length >= 3) {

			// takes care of the plus sign in the beggining
			if (phoneNumber[0] === '+') {

				// lets test the 00 chars after
				if (phoneNumber.slice(1, 3) === '00') {
					throw 'phoneNumber cannot start with + and have 00';
				} else if (phoneNumber[1] === ' ') {
					throw 'phoneNumber cannot have a whitespace after + ';
				} else {
					// remove + sign from the phoneNumber
					phoneNumber = phoneNumber.slice(1);
				}
			}
		}

		// removes all whitespaces
		phoneNumber = phoneNumber.replace(/\s/g, '');

		// if there are any chars at this time, this is not a valid number
		if (isNaN(phoneNumber)) {
			throw 'cannot have any symbol aside from the beginning + sign';
		}

		// if it starts with '00', these two digits don't count to the maximum number of digits
		var maxNumberOfDigits = (phoneNumber.slice(0, 2) === '00') ? 14 : 12;


		// has either 3 digits or between 7 and 12 digits (inclusive)
		if (((phoneNumber.length < 7) && (phoneNumber.length !== 3)) ||
			(phoneNumber.length > maxNumberOfDigits)) {
			throw 'phoneNumber doesnt have either 3 digits or between 7 and 12 digits (inclusive)';
		}

		// removes 00 from the phoneNumber if exists
		if (phoneNumber.slice(0, 2) === '00') {
			phoneNumber = phoneNumber.slice(2);
		}

		// returns a valid phone Number
		return phoneNumber;

	};

	// Parses an input file line by line (this way it can handle large files)
	// returns a Promise that is fullfiled after parsing the entire file
	var parseInputFile = function() {
		return new Promise(function(resolve, reject) {

			var instream = fs.createReadStream(inputFileName),
				outstream = new stream(),
				rl = readline.createInterface(instream, outstream);

			// fired at each phone number in file
			rl.on('line', function(line) {
				try {

					// parse and check area code
					var phoneNumber = parsePhoneNumber(line),
						areaCode = challengeApp.areaCodeModule.parsePhoneNumberAreaCode(phoneNumber);

					if (areaCode) {

						// if there is an area code increase it value

						if (phoneNumbersByAreaCodeList.hasOwnProperty(areaCode)) {
							phoneNumbersByAreaCodeList[areaCode]++;
						} else {
							// init entry for this area code
							phoneNumbersByAreaCodeList[areaCode] = 1;
						}
					} else if (challengeApp.config.REPORT_INVALID_NUMBERS) {
						// for test or debug purpose
						unknownAreaCodePhoneNumberList[line] = true;
					}
				} catch (err) {
					if (challengeApp.config.REPORT_INVALID_NUMBERS) {
						// for test or debug purpose
						inphoneNumbersByAreaCodeList[line] = err;
					}
				}

			});

			rl.on('close', function() {
				// at this time all phone number have been parsed
				resolve({
					phoneNumbersByAreaCodeList: phoneNumbersByAreaCodeList,
					inphoneNumbersByAreaCodeList: inphoneNumbersByAreaCodeList,
					unknownAreaCodePhoneNumberList: unknownAreaCodePhoneNumberList
				});
			});
		});
	};

	return {
		parseInputFile: parseInputFile
	};

})(challengeApp.inputFileName);


// it formats what is going to be shown in console
challengeApp.outputFormatter = function(phoneNumbersParsed) {
	var phoneNumbersByAreaCodeList = phoneNumbersParsed.phoneNumbersByAreaCodeList,
		inphoneNumbersByAreaCodeList = phoneNumbersParsed.inphoneNumbersByAreaCodeList,
		unknownAreaCodePhoneNumberList = phoneNumbersParsed.unknownAreaCodePhoneNumberList;

	// for test or debug purpose
	if (challengeApp.config.REPORT_INVALID_NUMBERS) {
		console.log('----- inphoneNumbersByAreaCodeList -----');
		console.log(inphoneNumbersByAreaCodeList);
		console.log('----- unknownAreaCodePhoneNumberList -----');
		console.log(Object.keys(unknownAreaCodePhoneNumberList));
		console.log('----- phoneNumbersByAreaCodeList -----');
	}

	// alphabetically sorted
	var sortedKeys = Object.keys(phoneNumbersByAreaCodeList).sort();

	// prints phone numbers aggregated by area code
	sortedKeys.forEach(function(key) {
		console.log(key + ':' + phoneNumbersByAreaCodeList[key]);
	});

};

// finally lets parse the file and print the values to the console
challengeApp.phoneNumberModule.parseInputFile()
	.then(challengeApp.outputFormatter)
	.catch(function(error) {
		console.log('error:', error);
	});